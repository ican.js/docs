### CAPITULO 2

% Redes alimentadas adiante com camada única, que possuem duas camadas, uma de entrada e outra de saída, sendo que, é nomeada camada única, por haver computações apenas na camada de saída \cite{Haykin2001}. Redes alimentadas diretamente com múltiplas camadas, estas possuem uma camada de entrada e uma de saída, assim como as redes alimentadas adiante, porém entre estas camadas podem existir uma ou diversas camadas escondidas \cite{Haykin2001}. Redes recorrentes
% \citeonline{Haykin2001} define a existência de três classes de arquiteturas fundamentais, sendo elas: Redes alimentadas adiante com camada única, Redes alimentadas diretamente com múltiplas camadas
% As redes alimentadas adiante com camada única são caracterizadas por possuirem básicamente 
% define a existência de três classes de arquiteturas fundamentais, sendo elas: Redes alimentadas adiante com camada única
%%%%% 17/02/2019
% Perguntar ao Orientador, se é necessário inserir mais informações sobre as arquiteturas de redes neurais.... Estou com dúvidas com relação
% a informações que podem acabar sendo desnecessárias ao trabalho.
% Talvez seja necessário colocar mais informações, simplesmente para eu ganhar mais embasamento
% nas explicações dos tópicos abaixo
% Fazendo isto, para o leitor passa a fazer sentido esta subseção
%%%%%
% Uma RNA pode ser dividida em três partes diferentes, estas denominadas de camadas
% (SILVA, SPATTI, FLAUZINO) -> Livro de redes neurais artificiais para engenharia
% Haykin (2001) -> Livro do Haykin (Redes neurais, principios e prática)
% Arquiteturas de RNAs definem a forma como seus diversos neurônios estão organizados, em relação uns aos outros (SILVA, SPATTI, FLAUZINO). Para Haykin (2001) há três classes de arquiteturas fundamentalmente diferentes, sendo elas:  Redes alimentadas adiante com camada única, às redes alimentadas diretamente com múltiplas camadas e às redes recorrentes.
% Onde, as redes alimentadas adiante com camada única são caracterizadas por possuirem uma camada de entrada, e uma camada de saída (Figura 3), onde ocorrem as computações dos resultados. São consideradas de camada única já que, as camadas de entrada, não realizam nenhum tipo de computação (Haykin, 2001). Dentre os principais tipos de redes desta arquitetura estão o \textit{Perceptron} e o \textit{Adeline}.
% Inserir a figura do Felipinho
% Já as arquiteturas de redes alimentadas diretamente, diferenciam-se das redes alimentadas adiante, por possuirem
% esta que se projeta sobre uma camada de saída (Figura 3)
% Eu estou deixando o texto da maneira como está, porém vou fazer mudanças depois (03/02/2019)
% Eu mudei muita coisa aqui, estava bastante incomodado com os resultados desta seção (03/02/2019)
% A arquitetura de uma RNA define como seus vários neurônios estão dispostos em relação um ao outro \cite{DaSilva2016}. Para Haykin (2001), existem três classes de arquiteturas de redes neurais fundamentalmente diferentes.
% (SILVA, SPATTI, FLAUZINO) -> Livro de redes neurais artificiais para engenharia
% Haykin (2001) -> Livro do Haykin (Redes neurais, principios e prática)
% Haykin (2001) define a existência de três classes de arquiteturas fundamentais, sendo elas: Redes alimentadas adiante com camada única
% Colocar abreviaturas nestes nomes ?
% \subsubsection{Redes alimentadas adiante com camada única}
% As redes alimentadas adiante com camada única (Figura 4) são a forma mais simples de uma rede, onde são apresentadas duas camadas, a camada de entrada, que básicamente recuperam os dados do meio externo e a camada de saída, esta constituida de neurônios que processam os dados e retornam os resultados, não havendo caminho inverso. 
% \image{1.0}{r_a_ad_com_mult_camadas.png}{Redes alimentadas adiante com camada única}{nn_per}
% Comentar a imagem ? ?
% \subsubsection{Redes alimentadas diretamente com múltiplas camadas}
% Falar aqui sobre a camada totalmente conectada é melhor! Acho mais interessante
% As redes alimentadas diretamente com múltiplas camadas (Figura 5) tem em sua estrutura a presença de uma ou mais camadas ocultas, onde estão presentes conjuntos de neurônios, que são básicamente são responsáveis por permitir que a rede extraia características mais complexas dos dados captados na camada de entrada, assim permitindo que a rede possa aprender características globais (CHURCHLAND; SEJNOWSKI, 1992), não ficando limitada apenas aos dados utilizado em seu treinamento.
% \image{1.0}{r_a_de_com_mult_camadas.png}{Redes alimentadas diretamente com múltiplas camadas}{nn_mlp}
% Comentar a imagem ? ?
% \subsubsection{Redes recorrentes}
% Em Redes recorrentes (Figura 6) existem as camadas de entrada e saída, porém nestas os valores de saída de um neurônio pode ser utilizado como entrada para sí próprio ou mesmo algum outro neurônio da mesma camada.
% \image{1.0}{r_rec.png}{Redes recorrentes}{nn_rr}
% \begin{itemize}
% 	\item Redes alimentadas adiante com camada única: Possuem a camada de entrada conectada unicamente a camada de saída;
% 	\item Redes alimentadas diretamente com múltiplas camadas: Apresentam camadas ocultas, que são utilizadas para extração de características mais complexas do contexto onde está inserida;
% 	\item Redes recorrentes: Operam com laços de realimentação, onde a saída de um neurônio é também sua entrada.
% \end{itemize}
% (SILVA; SPATTI; FLAUZINO, 2010) -> Livro
% Um exemplo muito comum de redes alimentadas adiante com camada única são os \textit{Perceptrons} (Figura 4), estas que basicamente são compostas pela camada de entrada, que não realizam operações nos dados de entrada e a camada de saída, que neste caso são compostas por conjuntos de neurônios, que operam sobre as saídas da camada de entrada e realizam as saídas da rede (SILVA; SPATTI; FLAUZINO, 2010).
% Para as redes alimentadas diretamente com múltiplas camadas, o exemplo comum são as \textit{Multi-Layers Perceptrons} (Figura 5)
%As RNAs podem ser constituídas de alguns ou mesmo vários neurônios artificiais, que são organizados através de camadas, estas que por sua vez definem a arquitetura da rede neural artificial. Para Haykin (2001) a arquitetura da rede neural é especifica para cada aplicação, e pode ser composta por diferentes níveis de camadas ocultas, função de ativação e algoritmo de aprendizado, podendo variar também a quantidade de neurônios que está sendo empregado na solução do problema.
%Mesmo havendo diversas variações de arquiteturas para diferentes problemas, normalmente encontra-se em uma RNA três tipos de camadas, sendo uma a camada de entrada, esta responsável em apenas receber os dados, uma camada oculta, que recebe os dados da camada de entrada e processa em um conjunto de neurônios e a camada de saída de resultados (PONTI, 2017). 
%Um exemplo de RNA que apresenta cada uma destas camadas citadas são as \textit{Multi-Layer Perceptron} (Figura 4), que além de possuirem uma camada de entrada e uma de saída, podem apresentar diversas camadas ocultas.
%\image{1.0}{mlp.png}{Rede de múltiplas camadas}{nn_mlp}
%Mas veja que, há arquiteturas que não seguem este formato, como é o caso das redes \textit{Perceptron} (Figura 5), que básicamente possuem apenas a camada de entrada e saída, e neste caso, todo o processamento ocorre na camada de saída.
%\image{1.0}{perceptron.png}{Rede de alimentação direta e camada simples}{nn_perceptron}
% Estudar mais sobre arquitetura de rede ! ! !
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \image{0.83}{nn_generic.png}{Rede neural artificial simples}{nn_generic}
% Mas há redes neurais artificiais como os \textit{Perceptrons} (Figura 5) que possuem apenas duas camadas, a camada de entrada, e a camada de saída, sendo que, o processamento dos dados neste tipo de rede é feito na camada de saída.
% \image{1.0}{perceptron.png}{Rede de alimentação direta e camada simples}{nn_perceptron}
% Buscar referências para a MLP
% E também poder haver redes com diversas camadas, como é os caso das \textit{Multi-Layer Perceptron} (Figura 6) que possuem as camadas de entrada e saída, e um conjunto de camadas ocultas que podem variar de uma única camada ou até mesmo dezenas delas, dependendo do problema a ser resolvido.
% \image{1.0}{mlp.png}{Rede de múltiplas camadas}{nn_mlp}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Em uma RNA simples, tem-se normalmente três camadas apenas (Figura 4), sendo uma a camada de entrada, esta responsável em apenas receber os dados, uma camada oculta, que recebe os dados da camada de entrada e processa em um conjunto de neurônios e a camada de saída de resultados (PONTI, 2017).
% Criar uma imagem que identifica cada uma das camadas que citei
% Acho que assim fica mais fácil entender o que está acontecendo
% Porém veja que, em arquiteturas simples como as redes \textit{Perceptron} (Figura 5), há apenas a camada de entrada e a camada de saída, não havendo camadas ocultas
% Este tópico vai ficar aqui, mas pode ser algo temporário, estou pensando sobre (02/02/2019)
% O que pensei foi, no momento que falar das camadas já falar da arquitetura de redes neurais, acho bacana fazer isto aqui
% As RNAs podem ser contituídas de alguns ou mesmo vários neurônios artificiais, que são organizados através de camadas, estes que por sua vez definem a arquitetura da rede neural. Para Haykin (2001), a arquitetura da rede 
% ToDo: Tentar buscar referência para confirmar esta afirmação do primeiro linha
% As RNAs podem ser contituídas de alguns ou mesmo vários neurônios artificiais, que são separados através de camadas, cada uma delas com uma função específica. Em uma RNA simples, tem-se normalmente três camadas apenas (Figura 4), sendo uma a camada de entrada, esta responsável em apenas receber os dados, uma camada oculta, que recebe os dados da camada de entrada e processa em um conjunto de neurônios e a camada de saída de resultados (PONTI, 2017).
% ToDo: Veja que isto deverá ser revisado
% Veja ainda que, a quantidade de camadas pode representar o poder de solução de problemas de uma rede, de acordo com PONTI (2017), RNAs de estruturas muito simples, como os \textit{Perceptrons}, que possuem apenas um neurônio, não são capazes de resolver problemas com complexidades não lineares.
% ToDo: Depois que comecei a ler sobre teoria do aprendizado estatístico, tudo o que eu escrevi não tem um total sentido, para trocar esta parte
% busque referências de aprendizado estatístico antes de continuar (02/02/2019)
% Através destes neurônios artificiais as RNAs são compostas com dezenas ou até mesmo centenas de neurônios, dependendo exclusivamente da complexidade do problema a ser resolvido. As RNA, ainda são divididas em camadas, onde cada uma delas tem uma função específica, uma RNA simples, normalmente tem-se três camadas apenas (Figura 4), uma camada de entrada, uma camada oculta, que contém um conjunto de neurônios e a camada de saída dos resultados.
% Na camada de entrada, os dados são recebidos e enviados para a camada oculta, na camada oculta, os dados são processados pelos neurônios, e seus resultados são unidos na camada de saída (PONTI, 2017).
% Um tipo muito comum de rede neural com esta estrutura básica são os chamados \textbf{Perceptrons}, que é uma rede neural simples, criada por Frank Rosenblat em 1957. Porém redes neurais com apenas uma camada de processamento (Camada oculta) não são aplicáveis em diversos casos, principalmente pela limitação no nível de complexidade de problemas que podem ser resolvidos com este tipo de rede (PONTI, 2017). Problemas como classificação de imagens, reconhecimento de objetos e detecção de fraudes podem ser um grande desafio para estes tipos de arquitetura, desta forma, mais camadas devem ser inseridas para tratamentos mais sofisticados dos dados, gerando resultados mais acertivos e principalmente, resolvendo problemas mais complexos. 
% Falar sobre as diferenças no processo de aprendizado (01/02/2019)
% Aqui falo de forma geral ? Preciso ir a fundo na questão ?? (01/02/2019)
% Talvez aqui eu possa introduzir erro, otimização, feed forward....estou pensando sobre isto (02/02/2019)
